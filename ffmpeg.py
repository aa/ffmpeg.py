#!/usr/bin/env python
from __future__ import print_function
from argparse import ArgumentParser
import sys, re, os, json, subprocess
from timecode import timecode_tosecs


def get_duration (text):
    m = re.search(r"Duration: *(\d\d:\d\d:\d\d\.\d+)", text, flags=re.I)
    if m:
        return timecode_tosecs(m.group(1))

def get_size (text):
    m = re.search(r"Stream #.+?Video.+?(\d\d+)x(\d\d+)", text, flags=re.I)
    if m:
        return int(m.group(1)), int(m.group(2))

def get_metadata (text):
    ret = {}
    for line in text.splitlines():
        if ':' in line:
            (name, value) = line.split(':', 1)
            if not name.endswith("http") and (name.upper() == name):
                ret[name.strip().lower().decode('utf-8', 'replace')] = value.strip().decode('utf-8', 'replace')
    return ret

def extract_frame(url, path, t=5.0):
    return subprocess.call(["ffmpeg", "-i", url, "-y", "-vframes", "1", "-ss", str(t), path])

def get_info(url):
    data = {}
    try:
        o = subprocess.check_output(["ffmpeg", "-i", url], stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        o = e.output

    d = get_duration(o)
    if d:
        data['duration'] = d 
    size = get_size(o)
    if size:
        data['size'] = size
        data['width'] = size[0]
        data['height'] = size[1]
    md = get_metadata(o)
    if md:
        data['metadata'] = md
    return data    


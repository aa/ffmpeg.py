from distutils.core import setup

setup(
    name='ffmpeg',
    version='0.1.0',
    author='Active Archives Contributors',
    scripts=['bin/ffinfo'],
    py_modules=['ffmpeg']
)

